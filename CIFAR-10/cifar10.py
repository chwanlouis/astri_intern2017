import numpy as np
import os
import pandas as pd
import time
from PIL import Image
import tensorflow as tf
import shutil

# 1. Run CIFAR10_train_test_split.py first
# 2. Make sure the value fo the data member (k_index) is same as CIFAR10_train_test_split.py

# Kaggle CIFAR-10 max accuracy: 0.7

class dataset(object):
    def __init__(self, is_remove_outliers=False):
        # python 2
        # self.all_string = string.digits + string.lowercase + string.uppercase
        # python 3
        self.all_string = ["airplane", "automobile", "bird", "cat", "deer", "dog", "frog", "horse", "ship", "truck"]
        self.training_path = 'all_path_text_file/all_train_path.csv'
        self.testing_path = 'all_path_text_file/all_test_path.csv'
        self.training_df = pd.read_csv(self.training_path)
        self.testing_df = pd.read_csv(self.testing_path)
        self.k_fold_training_path = 'all_path_text_file/k_fold_all_train_path.csv'
        self.kaggle_testing_path = 'all_path_text_file/kaggle_test_path.csv'

        # If don't want to use k_fold, just set it to False
        # And ignore k_index
        self.using_k_fold = True
        self.k_index = 10
        self.device = "/gpu:0"
        self.batch_size = 100
        self.learning_rate = 0.00085

        # If want need to submit to Kaggle, program will use the all_test_path.csv (which is according to TRAINING_RATIO)
        self.kaggle_subbmission = False

    def one_hot_encoding(self, label):
        '''
        Tranfer label to numpy array
        :param label: label(string) such as airplane,automobile
        :return: numpy array such as [1, 0, 0, 0, 0, 0 ,0 ,0 ,0 ,0 ]
        '''
        for i in range(len(self.all_string)):
            if label == self.all_string[i]:
                index = i
                return np.asarray([1 if i == index else 0 for i in range(len(self.all_string))], dtype=np.float32)

    def train_next_batch(self, n=100):
        '''
        only feeding training dataset
        :param n: batch size
        :return: tuple object ( x=numpy.ndarray(float32), y=numpy.ndarray(float32) )
        '''
        df_selected, self.training_df = self.training_df.iloc[:n], self.training_df.iloc[n:]
        length = len(df_selected)
        X = list()
        y = list()
        if length > 0:
            all_data = df_selected.values
            for row in all_data:
                file_path, label = row
                x = dataset.read_png(file_path).flatten()
                y_ = self.one_hot_encoding(label)
                X.append(x)
                y.append(y_)
            return (np.asarray(X, dtype=np.float32), np.asarray(y, dtype=np.float32))
        return None

    def test_next_batch(self, n=100):
        '''
        only feeding testing dataset
        :param n: batch size
        :return: tuple object ( x=numpy.ndarray(float32), y=numpy.ndarray(float32) )
        '''
        df_selected, self.testing_df = self.testing_df.iloc[:n], self.testing_df.iloc[n:]
        length = len(df_selected)
        X = list()
        y = list()
        if length > 0:
            all_data = df_selected.values
            for row in all_data:
                file_path, label = row
                x = dataset.read_png(file_path).flatten()
                y_ = self.one_hot_encoding(label)
                X.append(x)
                y.append(y_)
            return (np.asarray(X, dtype=np.float32), np.asarray(y, dtype=np.float32))
        return None

    @staticmethod
    def read_png(file_path):
        img = Image.open(file_path).convert("RGB")
        img_data = img.getdata()
        img_as_list = np.asarray(img_data, dtype=float)
        img_as_list = img_as_list.reshape(img.size[0], img.size[1], 3)
        return np.asarray(img_as_list, dtype=np.float32)

    def get_k_index(self):
        '''

        :return: k_index
        '''
        return self.k_index

    def set_texting_path_for_kaggle(self):
        '''
        set_texting_path_for_kaggle
        :return:
        '''
        self.testing_path = self.kaggle_testing_path
        self.testing_df = pd.read_csv(self.testing_path)
        return

    def set_training_path(self):
        '''
        for k-fold
        set function to change the training_path.csv
        :return:
        '''
        self.training_path = self.k_fold_training_path
        self.training_df = pd.read_csv(self.training_path)
        return


    def train_k_fold(self, writer):
        '''
        Do the iteration k times
        Each part of the data should be the testing set once.
        And other data be the training set
        :param writer: writer is just for tensorboard
        :return: training accuracy
        '''

        # Get the k value first
        # Group all the training data from each "trainx.csv" to a k_fold_training_path.csv
        for i in range(self.get_k_index()):
            with open(self.k_fold_training_path, 'w') as writing_file:
                writing_file.write('path,label\n')
                for j in range(self.get_k_index()):
                    if (j == i):
                        continue
                    file_path = "all_path_text_file/train" + str(j + 1) + ".csv"
                    df = pd.read_csv(file_path)
                    all_data = df.values
                    for row in all_data:
                        file_path, label = row
                        writing_file.write('%s,%s\n' % (file_path, label))

            # Set the k_fold_training_path to be the training_path
            # Then call the train_next_batch to get the next 100data
            # then keep training
            self.set_training_path()
            print("----------------------------------------------------------")
            print("train%d.csv as testing set, other file as training set" % (i))
            batch = self.train_next_batch(data_feeder.batch_size)
            k = data_feeder.batch_size
            while batch is not None:
                if k % (data_feeder.batch_size*10) == 0:
                    train_accuracy = accuracy.eval(feed_dict={x: batch[0], y_: batch[1], keep_prob: 1.0})
                    print('train%d.csv as testing set. ' % i)
                    print('step %d, training accuracy %g' % (k, train_accuracy))
                _, loss = sess.run([train_step, cross_entropy], feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
                writer.add_summary(_, k)
                print("loss: ", loss)
                batch = data_feeder.train_next_batch(data_feeder.batch_size)
                k += data_feeder.batch_size
                # Then for loop k times
        return


    def train_original_version(self, writer):
        '''

        :param writer: writer is only for tensorboard
        :return:
        '''
        batch = self.train_next_batch(data_feeder.batch_size)
        i = data_feeder.batch_size
        while batch is not None:
            if i % (data_feeder.batch_size*10) == 0:
                train_accuracy = accuracy.eval(feed_dict={x: batch[0], y_: batch[1], keep_prob: 1.0})
                print('step %d, training accuracy %g' % (i, train_accuracy))
            _, loss = sess.run([train_step, cross_entropy], feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
            writer.add_summary(_, i)
            print('loss:', loss)
            batch = self.train_next_batch(data_feeder.batch_size)
            i += data_feeder.batch_size
        return


# CNN
def cnn_model_with_5_layers(x):
    '''
    :param x: input tensor with the dimensions [None, 16384]
    :return: tuple (y_conv, keep_prob)
             where y_conv is the logits, keep_prob is the placeholder for the dropout rate
    '''
    x_image = tf.reshape(x, [-1, 32, 32, 3])

    # size: 32x32
    with tf.variable_scope('conv1') as scope:
        W_conv1 = weight_variable([2, 2, 3, 64], 5e-2)
        b_conv1 = bias_variable(0.0, [64])
        h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)
        tf.summary.histogram(scope.name + '/activations', h_conv1)

    with tf.variable_scope('pooling1_lrn') as scope:
        h_pool1 = tf.nn.max_pool(h_conv1, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1],
                                 padding='SAME', name='pooling1')

    # size: 16x16
    with tf.variable_scope('conv2') as scope:
        W_conv2 = weight_variable([2, 2, 64, 64], 5e-2)
        b_conv2 = bias_variable(0.1, [64])
        h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)
        tf.summary.histogram(scope.name + '/activations', h_conv2)

    with tf.variable_scope('pooling2_lrn') as scope:
        h_norm2 = tf.nn.lrn(h_conv2, depth_radius=4, bias=1.0, alpha=0.001 / 9.0,
                            beta=0.75, name='norm2')
        h_pool2 = tf.nn.avg_pool(h_norm2, ksize=[1, 3, 3, 1], strides=[1, 2, 2, 1],
                                 padding='SAME', name='pooling2')

    # size: 8x8
    with tf.variable_scope('local3') as scope:
        W_fc1 = weight_variable([8 * 8 * 64, 384], 0.04)
        b_fc1 = bias_variable(0.1, [384])
        h_pool2_flat = tf.reshape(h_pool2, [-1, 8 * 8 * 64])
        h_fc1 = tf.nn.relu(tf.matmul(h_pool2_flat, W_fc1) + b_fc1)

        keep_prob = tf.placeholder(tf.float32)
        h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)
        tf.summary.histogram(scope.name + '/activations', h_fc1)

    with tf.variable_scope('local4') as scope:
        W_fc2 = weight_variable([384, 192], 0.04)
        b_fc2 = bias_variable(0.1, [192])
        h_fc2 = tf.nn.relu(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)
        tf.summary.histogram(scope.name + '/activations', h_fc2)
        h_fc2_drop = tf.nn.dropout(h_fc2, keep_prob)

    with tf.variable_scope('softmax_linear') as scope:
        W_fc3 = weight_variable([192, 10], 1 / 192.0)
        b_fc3 = bias_variable(0.0, [10])
        y_conv = tf.matmul(h_fc2_drop, W_fc3) + b_fc3
        tf.summary.histogram(scope.name + '/activations', y_conv)
    return y_conv, keep_prob


def conv2d(x, W):
    """conv2d returns a 2d convolution layer with full stride."""
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x):
    """max_pool_2x2 downsamples a feature map by 2X."""
    return tf.nn.max_pool(x, ksize=[1, 3, 3, 1],
                          strides=[1, 2, 2, 1], padding='SAME')


def avg_pool_2x2(x):
    return tf.nn.avg_pool(x, ksize=[1, 3, 3, 1],
                          strides=[1, 2, 2, 1], padding='SAME')


def weight_variable(shape, stddev):
    """weight_variable generates a weight variable of a given shape."""
    initial = tf.truncated_normal(shape, stddev=stddev)
    return tf.Variable(initial)


def bias_variable(cons, shape):
    """bias_variable generates a bias variable of a given shape."""
    initial = tf.constant(cons, shape=shape)
    return tf.Variable(initial)


if __name__ == '__main__':
    data_feeder = dataset()
    with tf.device(data_feeder.device):

        # Create the model
        x = tf.placeholder(tf.float32, [None, 3072])

        # Define loss and optimizer
        y_ = tf.placeholder(tf.float32, [None, 10])

        # Build the graph for the deep net
        y_conv, keep_prob = cnn_model_with_5_layers(x)
        y_predict = tf.nn.softmax(y_conv)

        with tf.name_scope('loss'):
            cross_entropy = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))
            tf.summary.scalar('loss', cross_entropy)

        with tf.name_scope('train'):
            train_step = tf.train.AdamOptimizer(data_feeder.learning_rate).minimize(cross_entropy)
        correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    startTime = time.time()

    print("Start Time: ", startTime)
    with tf.Session(config=tf.ConfigProto(log_device_placement=True, allow_soft_placement = True)) as sess:
        # For Tensorboard
        merged = tf.summary.merge_all()
        writer = tf.summary.FileWriter("logs/", sess.graph)

        sess.run(tf.global_variables_initializer())
        # If dont want to implement with k_fold
        # Can change line 332 to data_feeder.train_default()
        if(data_feeder.using_k_fold == True):
            data_feeder.train_k_fold(writer)
        else:
            data_feeder.train_original_version(writer)
        test_size = list()
        test_acc = list()
        test_prediction = list()
        test_actual = list()

        # Set the texting path for kaggle usage
        # If u want to change the file to the all_test_path.csv, just delete line399
        if(data_feeder.kaggle_subbmission == True):
            data_feeder.set_texting_path_for_kaggle()
        test_batch = data_feeder.test_next_batch(data_feeder.batch_size)
        i = data_feeder.batch_size
        while test_batch is not None:

            # acc = accuracy.eval(feed_dict={x: test_batch[0], y_: test_batch[1], keep_prob: 1.0})
            acc, y_likelihood = sess.run([accuracy, y_predict],
                                         feed_dict={x: test_batch[0], y_: test_batch[1], keep_prob: 1.0})

            # Get back the result such accuracy, likelihood
            test_acc.append(acc)
            test_size.append(len(test_batch[0]))
            test_prediction.append(y_likelihood)

            if i % (data_feeder.batch_size*10) == 0:
                print('%dth test accuracy %g' % (i, acc))
            test_batch = data_feeder.test_next_batch(data_feeder.batch_size)
            i += data_feeder.batch_size

        all_test_acc = np.average(test_acc, weights=[float(i) / sum(test_size) for i in test_size])
        print('overall test accuracy %g' % all_test_acc)
    endTime = time.time()
    print("End Time: ", endTime)
    print("Time Used: ", endTime - startTime)
    num = data_feeder.batch_size

    # Get back the y_likelihood
    # And use string to represent
    test_prediction_type_name = []
    for i in range(len(test_size)):
        if i == (len(test_size) - 1):
            num = test_size[len(test_size) - 1]
        for j in range(num):
            index = np.argmax(test_prediction[i][j])
            test_prediction_type_name.append(data_feeder.all_string[index])

    # Get back the ID only(such as 22882 from dataset/photo/22882.png)
    test_path_id = []
    if (data_feeder.kaggle_subbmission == True):
        target_file = data_feeder.kaggle_testing_path
    else:
        target_file = data_feeder.testing_path
    fo = open(target_file, "r")
    print("Name of the file: ", fo.name)
    next(fo)
    for line in fo:
        line, label = line.split(',')
        a, b, id = line.split('/')
        id, png = id.split('.')
        test_path_id.append(id)
    fo.close()

    # Then format the output for the kaggle submission
    with open('all_path_text_file/result.csv', 'w') as file:
        file.write('id,label\n')
        for i in range(len(test_path_id)):
            file.write('%s,%s\n' % (test_path_id[i], test_prediction_type_name[i]))
    print("result.csv is generated")

    if (data_feeder.kaggle_subbmission == False):
        # Get all the testing set labels first
        target_file = data_feeder.testing_path
        img_test_path = []
        actual_label = []
        fo = open(target_file, "r")
        print("Name of the file: ", fo.name)
        next(fo)
        for line in fo:
            line, label = line.split(',')
            label = label.split('\n')
            label = label[0]
            #label = label[:-1]
            img_test_path.append(line)
            actual_label.append(label)
        fo.close()

        # Create the folder if not be created
        if not os.path.isdir('wrong_estimation'):
            os.mkdir('wrong_estimation')
        for i in range(len(data_feeder.all_string)):
            if not os.path.isdir(('wrong_estimation/%s')% str(data_feeder.all_string[i])):
                os.mkdir(('wrong_estimation/%s')% str(data_feeder.all_string[i]))

        # Copy the wrong estimation image with the new file name
        # New file name: actualLabel_photoID_predictionLabel.png
        with open('wrong_estimation/path_actual_estimation.csv', 'w') as writingfile:
            writingfile.write('path,actual_label,estimation_label\n')
            for i in range(len(img_test_path)):
                if(actual_label[i] != test_prediction_type_name[i]):
                    writingfile.write('%s,%s,%s\n' % (img_test_path[i], actual_label[i], test_prediction_type_name[i]))
                    new_file_name = ("wrong_estimation/%s/%s_%s_%s.png")% (actual_label[i], actual_label[i], test_path_id[i], test_prediction_type_name[i])
                    original_img_path = img_test_path[i]
                    shutil.copyfile(original_img_path, new_file_name)
        print("path_actual_estimation.csv is generated")

        # Count the number of wrong prediction of the class
        num_of_row =[]
        with open('wrong_estimation/count_row.csv', 'w') as file:
            file.write('calss,number of img cannot predict this actual class\n')
            for i in range(len(data_feeder.all_string)):
                total = 0
                file_name = ('wrong_estimation/%s') % str(data_feeder.all_string[i])
                for root, dirs, files in os.walk(file_name):
                    total += len(files)
                num_of_row.append(total)
            for i in range(len(data_feeder.all_string)):
                file.write("%s,%s\n"%(data_feeder.all_string[i],num_of_row[i]))
        print("count_row.csv is generated")