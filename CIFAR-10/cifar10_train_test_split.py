import os
import csv
import pandas as pd
import numpy as np

# CIFAR-10 Dataset information:
#   - 32x32 color images
#   - 60,000 images
#   - 10 label classes
#       - including:  airplane, automobile, bird, cat, deer, dog, frog, horse, ship, truck
#   - 6,000 images per class


#########################################################################################################
#                            1.  Download data
#########################################################################################################
#   Download training data
#       There are 50,000 training images in the official data.
#       Download link: https://www.kaggle.com/c/cifar-10/data
#          - train.7z(containing the training images in png format
#          - trainLabels.csv (training labels)
#       p.s.in this program the training data have split into two parts as training set and testing set
#
#   Download Testing data for Kaggle competitions.
#       There are 10,000 testing images in the official data.
#       However, Kaggle have added 290,000 junk images (prevent cheating)
#       Download link: Kaggle - test.7z(containing the training images in png format
#          - https://www.kaggle.com/c/cifar-10/data
#
#  Then, following the below format:
#
#  cifar10/
#  |- dataset/
#  |  |- photo/
#  |     |- 1.jpg/
#  |     |- 2.jpg/
#  |     |- .../
#  |     |- 50000.jpg/
#  |  |- test/
#  |     |- 1.jpg/
#  |     |- 2.jpg/
#  |     |- .../
#  |     |- 300000.jpg/
#  |  |-photoLabels,csv


#########################################################################################################
#                            2. Get the path text file
#########################################################################################################
# Run this program
# Config:
#   TRAINING_RATIO = x (e.g 0.7)
#   split_k_part(k) (e.g 10)
#
# And these folders and files will be created for you
#  cifar10/
#  |- all_path_text_file
#     |- all_path.csv
#     |- all_text_path.csv
#     |- all_train_path.csv
#     |- kaggle_test_path.csv
#     |- train1.csv
#     |- train2.csv
#     |- ...
#     |- traink.csv


def generate_all_file_path():
    '''
    Get all the photos file name and the corresponding label of the photo
    Store the information in the csv file
    :return: a csv file called all_path.csv is created or modified
    '''
    if not os.path.isdir('all_path_text_file'):
        os.mkdir('all_path_text_file')
    target_writing_file = 'all_path_text_file/all_path.csv'

    # Get all the string after ',' every line(include header) from the 'dataset/photoLabels.csv'
    # Then append to all_labels
    # Results: all_labels = [label, frog, truck, ..., automobile]
    all_labels = []
    with open('dataset/photoLabels.csv', newline='') as csvfile:
        reader = csv.reader(csvfile, delimiter=' ', quotechar='|')
        for row in reader:
            line = ','.join(row)
            id, label = line.split(',')[0], line.split(',')[1]
            all_labels.append(label)

    # Scan all the photo and get the file name
    # Store the photo file name and the corresponding label in the target_writing_file
    with open(target_writing_file, 'w') as writing_file:
        writing_file.write('path,label\n')
        pathway_to_search = 'dataset/photo'
        for root, dirs, files in sorted(os.walk(pathway_to_search)):
            dirs.sort()
            for file_path in files:
                photo_id = int(file_path.split('.')[0])
                string = '%s/%s,%s\n' % (pathway_to_search, file_path, all_labels[photo_id])
                writing_file.write(string)
    print('%s generated' % target_writing_file)


def train_test_split(training_ratio):
    '''
    Split all the data in two part(training set and testing set) based on training rate
    :param training_ratio:
    :return: two file(all_test_path, all_train_path) are created or modified
    '''
    directory = 'all_path_text_file'
    all_train_df = None
    all_test_df = None

    # Read the all_path.csv
    # Store the data separately in the array based on rand_index
    file_path = '%s/all_path.csv' % (directory)
    print('reading %s' % file_path)
    df = pd.read_csv(file_path)
    rand_index = np.random.rand(len(df)) < training_ratio
    train_df = df[rand_index]

    test_df = df[~rand_index]
    if all_train_df is None:
        all_train_df = train_df
        all_test_df = test_df
    else:
        all_train_df = all_train_df.append(train_df)
        all_test_df = all_test_df.append(test_df)

    # Write the two array in two corresponding csv
    all_train_fname = '%s/all_train_path.csv' % directory
    all_test_fname = '%s/all_test_path.csv' % directory

    all_train_df = all_train_df.sample(frac=1)
    all_test_df = all_test_df.sample(frac=1)

    all_train_df.to_csv(all_train_fname, index=None)
    all_test_df.to_csv(all_test_fname, index=None)

    print("all_path_text_file/all_train_path.csv generated")
    print("all_path_text_file/all_test_path.csv generated")


def split_k_part(k):
    '''
    Separate the all_train_path.csv into k csv file
    :param k:
    :return: k numbers of csv is generated (e.g train1.csv, train2.csv, ..., train10.csv
    '''
    all_training_path_csv = 'all_path_text_file/all_train_path.csv'
    training_df = pd.read_csv(all_training_path_csv)

    with open(all_training_path_csv, "r") as f:
        reader = csv.reader(f, delimiter=",")
        data = list(reader)
        row_count = len(data)

    print("Number of row of all_train_path.csv: ",row_count)
    num_of_rows = row_count//k
    print("Each csv num of rows: ",num_of_rows)
    for i in range(k):
        string = "train"+ str(i+1)
        df_selected, training_df = training_df.iloc[:num_of_rows], training_df.iloc[num_of_rows:]
        length = len(df_selected)
        if length > 0:
            all_data = df_selected.values
            #if (Path('all_path_text_file/'+string+'.csv').is_file() == False):
            with open('all_path_text_file/'+string+'.csv', 'w') as file:
                file.write('path,label\n')
                for row in all_data:
                    file_path, label = row
                    file.write('%s,%s\n' % (file_path, label))
                print('all_path_text_file/' + string + '.csv is generated')


def create_kaggle_text_path():
    '''
    Scan all the files in the photo dataset file
    Add the hardcode label to all photo (airplane used) because want to match the format of the model
    Write the string in the target_writing_file called "all_path_text_file/kaggle_test_path.csv"
    :return: "all_path_text_file/kaggle_test_path.csv" is created or modified
    '''

    target_writing_file ="all_path_text_file/kaggle_test_path.csv"
    with open(target_writing_file, 'w') as writing_file:
        writing_file.write('path,label\n')
        pathway_to_search = 'dataset/test'
        for root, dirs, files in sorted(os.walk(pathway_to_search)):
            dirs.sort()
            for file_path in files:
                photo_id = int(file_path.split('.')[0])
                string = '%s/%s,airplane\n' % (pathway_to_search, file_path)
                writing_file.write(string)
    print('%s generated' % target_writing_file)


if __name__ == '__main__':
    TRAINING_RATIO = 0.8
    generate_all_file_path()
    train_test_split(TRAINING_RATIO)
    split_k_part(10)
    create_kaggle_text_path()


