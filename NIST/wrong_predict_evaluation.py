import os
from PIL import Image


def wrong_predict_evaluation(wrong_prediction_log_filename, label_string):
    """
    copy the data which are wrong predicted from NIST dataset to a new directory,
    and generate a matrix counting the number of wrong predictions
    :param wrong_prediction_log_filename: csv file path of the wrong_prediction_log file
    :param label_string: possible outcomes of prediction
    :return: None
    """

    # make a new directory for storing the wrongly predicted data
    if not os.path.isdir('wrong_prediction'):
        os.mkdir('wrong_prediction')

    base = os.path.basename(wrong_prediction_log_filename)
    directory = 'wrong_prediction/%s' % os.path.splitext(base)[0]
    if not os.path.isdir(directory):
        os.mkdir(directory)

    # generate a matrix for counting the number of wrong predictions
    matrix = [[0 for j in range(len(label_string) + 1)] for i in range(len(label_string) + 1)]
    matrix[0][0] = '-'
    for i in range(len(label_string)):
        matrix[0][i + 1] = label_string[i]
        matrix[i + 1][0] = label_string[i]

    with open(wrong_prediction_log_filename, 'r') as file:
        file.readline()
        lines = file.readlines()
        for row in lines:
            # read the csv files, and copy the data via the paths recorded in csv files
            photo_path, wrong_predict, actual_label = row.split(', ')
            actual_label = actual_label.split()[0]
            img = Image.open(photo_path)
            photo_basename = os.path.basename(photo_path)
            result_name = '%s_%s_%s' % (actual_label, wrong_predict, photo_basename)
            img.save('%s/%s' % (directory, result_name))

            # count the number of wrong prediction
            wrong_predict_index = label_string.index(wrong_predict)
            actual_label_index = label_string.index(actual_label)
            matrix[actual_label_index + 1][wrong_predict_index + 1] += 1

    # generate a csv file which store the counting matrix
    log_filename = '%s/wrong_prediction_matrix.csv' % directory
    with open(log_filename, 'w') as file:
        for i in range(len(matrix)):
            for j in range(len(matrix[i])):
                file.write('%s' % matrix[i][j])
                if j != len(matrix[i]) - 1:
                    file.write(',')
            file.write('\n')

    # generate a ReadMe file
    readme_filename = 'wrong_prediction/readme.txt'
    with open(readme_filename, 'w') as file:
        file.write('The folders under this directory store the data from NIST dataset that are predicted wrongly.\n')
        file.write('The filename of each data state the actual label and the wrong prediction of a datum.\n')
        file.write('\te.g. For the filename "A_P_hsf_1_00084.png":\n')
        file.write('\t     the first letter "A" is the actual label of the datum,\n')
        file.write('\t     while the second letter "P" is the wrong prediction generated by the neural network.\n')
        file.write('\n')
        file.write('The file "wrong_prediction_matrix.csv" under each folders counts the number of wrong prediction.\n')
        file.write('The vertical axis represents the actual label')
        file.write(' while the horizontal axis represents the wrong prediction.\n')
        file.write('\te.g. For a matrix like this:\n')
        file.write('\t\t - A B C\n')
        file.write('\t\t A 0 5 0\n')
        file.write('\t\t B 1 0 2\n')
        file.write('\t\t C 1 1 0\n')
        file.write('\tThere are 5 "A"s which are wrongly predicted as "B" by the neural network.')
