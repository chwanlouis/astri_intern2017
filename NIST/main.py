#################################################################################
# NIST dataset classification using CNN                                         #
# with around 95% accuracy                                                      #
#                                                                               #
# Package required:                                                             #
#   - numpy                                                                     #
#   - pandas                                                                    #
#   - PIL (Python Image Library)                                                #
#   - python-resize-image                                                       #
#   - tensorflow/tensorflow-gpu (version 0.12 or higher)                        #
#                                                                               #
# Instruction:                                                                  #
#   1. Download the NIST Standard Dataset 19 from                               #
#       https://s3.amazonaws.com/nist-srd/SD19/by_class.zip                     #
#       and then extract it to the local directory                              #
#   2. Double check                                                             #
#       a. version of tensorflow                                                #
#       b. global variable 'DIRECTORY' in 'NIST_train_test_split.py' as well as #
#           'nist.py'                                                           #
#   3. Tune the parameters (NOT the global variable) in 'main.py' (this file)   #
#   4. Run this script, and then the program would train and test the model     #
#                                                                               #
#################################################################################

import nist
import NIST_train_test_split
import os
from time import localtime, strftime

# Do not modify these global variables
SUBSET = ['digits', 'uppercase_letters', 'lowercase_letters', 'letters_with_ambiguous_merged', 'all_letters', \
          'digits_and_ambiguous_merged_letters', 'digits_and_letters']
NUM_OF_CLASS = {'digits': 10, 'uppercase_letters': 26, 'lowercase_letters': 26, 'letters_with_ambiguous_merged': 37, \
                'all_letters': 52, 'digits_and_ambiguous_merged_letters': 47, 'digits_and_letters': 62}


if __name__ == '__main__':
    # training_ratio: Determine the proportion of training dataset to testing dataset
    # learning_rate: Store the learning rates that would like to be examined into a list,
    #                   the best learning rate for this model is around 0.0009
    # iteration: Determine the frequency of the experiment
    # directory: Determine the location of log files
    # device: device where implement the TensorFlow operations
    training_ratio = 0.7
    learning_rate = [0.0009]  # other values, like 0.0007, can be appended
    iteration = 1
    directory = 'logs'
    '''
    Note: 
    If tensorflow could not support GPU implementation, then do not modify the parameter 'device';
    If not, '/gpu:0' can be appended into the list, or just change '/cpu:0' to '/gpu:0'.
    '''
    device = ['/cpu:0']

    # Generate the path files, stored in the folder 'all_path_text_file' by default
    NIST_train_test_split.generate_all_file_path()
    NIST_train_test_split.train_test_split(training_ratio)

    if not os.path.isdir(directory):
        os.mkdir(directory)

    for i in range(iteration):
        for lr in learning_rate:
            for de in device:
                log_prefix = ''
                if de == '/cpu:0':
                    log_prefix = '%s/cpu_lr%s_%dth' % (directory, lr, i)
                elif de == '/gpu:0':
                    log_prefix = '%s/gpu_lr%s_%dth' % (directory, lr, i)
                for j in range(len(SUBSET)):
                    print('-'*40)
                    print('\n[%s] Running the %dth time on %s\nDataset: %s\n' % (strftime("%Y-%m-%d %H:%M:%S", localtime()), i + 1, de, SUBSET[j]))
                    log_filename = '%s_%s' % (log_prefix, SUBSET[j])
                    nist.main(de, log_filename, SUBSET[j], NUM_OF_CLASS[SUBSET[j]], lr)
