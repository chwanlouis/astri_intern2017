import os
import pandas as pd
import numpy as np

# Do not modify these global variables, which are storing the folder names of the data
ALL_SUB_FOLDER = ['hsf_0', 'hsf_1', 'hsf_2', 'hsf_3', 'hsf_4', 'hsf_6', 'hsf_7']
ALL_DIGITS = ['30', '31', '32', '33', '34', '35', '36', '37', '38', '39']
ALL_UPPERCASE_LETTERS = ['41', '42', '43', '44', '45', '46', '47', \
                         '48', '49', '4a', '4b', '4c', '4d', '4e', \
                         '4f', '50', '51', '52', '53', '54', '55', \
                         '56', '57', '58', '59', '5a']
ALL_LOWERCASE_LETTERS = ['61', '62', '63', '64', '65', '66', '67', \
                         '68', '69', '6a', '6b', '6c', '6d', '6e', \
                         '6f', '70', '71', '72', '73', '74', '75', \
                         '76', '77', '78', '79', '7a']
ALL_LETTERS = ALL_UPPERCASE_LETTERS + ALL_LOWERCASE_LETTERS
ALL_DIGITS_AND_LETTERS = ALL_DIGITS + ALL_UPPERCASE_LETTERS + ALL_LOWERCASE_LETTERS

ALL_ASCII = [ALL_DIGITS, ALL_UPPERCASE_LETTERS, ALL_LOWERCASE_LETTERS, ALL_LETTERS, ALL_DIGITS_AND_LETTERS]
TYPE = ['all_digits', 'all_uppercase_letters', 'all_lowercase_letters', 'all_letters', 'all_digits_and_letters']

'''
The global variable 'DIRECTORY' locates the directory where the csv files would be stored
WARNING: Make sure to double check the paths of dataset in 'nist.py' once this variable is modified
'''
DIRECTORY = 'all_path_text_file'


def generate_all_file_path():
    """
    :return: 62 csv files (52 for english alphabets and 10 for digits) recording the file path and label of each data
    """
    if not os.path.isdir(DIRECTORY):
        os.mkdir(DIRECTORY)
    for i in range(len(ALL_ASCII)):
        for ascii_code in ALL_ASCII[i]:
            target_writing_file = '%s/class_%s_path.csv' % (DIRECTORY, ascii_code)
            with open(target_writing_file, 'w') as writing_file:
                writing_file.write('path,label\n')
                for sub_folder in ALL_SUB_FOLDER:
                    pathway_to_search = 'by_class/%s/%s' % (ascii_code, sub_folder)
                    all_file_name_list = list(os.walk(pathway_to_search))[0][2]
                    for file_name in all_file_name_list:
                        string = '%s/%s,%s\n' % (pathway_to_search, file_name, chr(int(ascii_code, 16)))
                        writing_file.write(string)
            print('%s generated' % target_writing_file)


def train_test_split(training_ratio):
    """
    :param training_ratio: Determine the proportion of training dataset to testing dataset
    :return: csv files for training dataset and testing dataset
    """
    all_train_df = None
    all_test_df = None
    for i in range(len(ALL_ASCII)):
        for ascii_code in ALL_ASCII[i]:
            file_path = '%s/class_%s_path.csv' % (DIRECTORY, ascii_code)
            print('reading %s' % file_path)
            df = pd.read_csv(file_path)

            # Split the paths into two parts, one for training dataset and the other one is testing dataset
            rand_index = np.random.rand(len(df)) < training_ratio
            train_df = df[rand_index]
            test_df = df[~rand_index]
            if all_train_df is None:
                all_train_df = train_df
                all_test_df = test_df
            else:
                all_train_df = all_train_df.append(train_df)
                all_test_df = all_test_df.append(test_df)

            # Write the paths into csv separately based on their classes
            train_fname = '%s/class_%s_train_path.csv' % (DIRECTORY, ascii_code)
            test_fname = '%s/class_%s_test_path.csv' % (DIRECTORY, ascii_code)
            train_df.to_csv(train_fname, index=None)
            test_df.to_csv(test_fname, index=None)
            print('%s train and test split' % file_path)

        all_train_fname = '%s/%s_train_path.csv' % (DIRECTORY, TYPE[i])
        all_test_fname = '%s/%s_test_path.csv' % (DIRECTORY, TYPE[i])

        # Shuffle the order of paths before writing them into csv file
        # in order to prevent overfitting during training progress
        all_train_df = all_train_df.sample(frac=1)
        all_test_df = all_test_df.sample(frac=1)

        all_train_df.to_csv(all_train_fname, index=None)
        all_test_df.to_csv(all_test_fname, index=None)

        df.drop(df.index, inplace=True)
        all_train_df.drop(all_train_df.index, inplace=True)
        all_test_df.drop(all_test_df.index, inplace=True)


if __name__ == '__main__':
    TRAINING_RATIO = 0.7
    generate_all_file_path()
    train_test_split(TRAINING_RATIO)
