import string
import numpy as np
import pandas as pd
from PIL import Image
import PIL.ImageOps
from resizeimage import resizeimage
from time import time
import tensorflow as tf
from wrong_predict_evaluation import wrong_predict_evaluation


# For debugging syntactical error conveniently
DEBUG_FLAG = False

# Do not modify these two global variables
SUBSET = ['digits', 'uppercase_letters', 'lowercase_letters', 'letters_with_ambiguous_merged', 'all_letters', \
          'digits_and_ambiguous_merged_letters', 'digits_and_letters']
TYPE = ['all_digits', 'all_uppercase_letters', 'all_lowercase_letters', 'all_letters', 'all_digits_and_letters']

# A list storing the letters that are ambiguous with their cases
AMBIGUOUS_LETTERS = ['C', 'I', 'J', 'K', 'L', 'M', 'O', 'P', 'S', 'U', 'V', 'W', 'X', 'Y', 'Z']

'''
The global variable 'DIRECTORY' locates the directory where the csv path files stored
WARNING: Make sure to double check the directory in 'NIST_train_test_split.py' once this variable is modified
'''
DIRECTORY = 'all_path_text_file'


class Dataset(object):
    """
    This reads the data from the paths stored in csv files for training and testing the model
    data members:
        - ambiguous_merged: letters in the list AMBIGUOUS_LETTERS would become case-insensitive if True
        - all_string: all classes of the dataset
        - training_path: csv file path of training dataset
        - test_path: csv file path of testing dataset
        - outliers_path: csv file path of outliers
        - training_df: data frame of training data
        - testing_df: data frame of testing data
    member functions:
        - one_hot_encoding: convert the label into one-hot vector
        - train_next_batch: grab n data from training_df
        - test_next_batch:  grab n data from testing_df
        - read_png: a helper function which read png files and return them as numpy array

    """
    def __init__(self, subset, is_remove_outliers=False):
        if subset == SUBSET[0]:
            self.ambiguous_merged = False
            self.all_string = string.digits
            self.training_path = '%s/all_digits_train_path.csv' % DIRECTORY
            self.testing_path = '%s/all_digits_test_path.csv' % DIRECTORY
        elif subset == SUBSET[1]:
            self.ambiguous_merged = False
            self.all_string = string.ascii_uppercase
            self.training_path = '%s/all_uppercase_letters_train_path.csv' % DIRECTORY
            self.testing_path = '%s/all_uppercase_letters_test_path.csv' % DIRECTORY
        elif subset == SUBSET[2]:
            self.ambiguous_merged = False
            self.all_string = string.ascii_lowercase
            self.training_path = '%s/all_lowercase_letters_train_path.csv' % DIRECTORY
            self.testing_path = '%s/all_lowercase_letters_test_path.csv' % DIRECTORY
        elif subset == SUBSET[3]:
            self.ambiguous_merged = True
            self.all_string = [x for x in string.ascii_letters if x not in AMBIGUOUS_LETTERS]
            self.training_path = '%s/all_letters_train_path.csv' % DIRECTORY
            self.testing_path = '%s/all_letters_test_path.csv' % DIRECTORY
        elif subset == SUBSET[4]:
            self.ambiguous_merged = False
            self.all_string = string.ascii_lowercase + string.ascii_uppercase
            self.training_path = '%s/all_letters_train_path.csv' % DIRECTORY
            self.testing_path = '%s/all_letters_test_path.csv' % DIRECTORY
        elif subset == SUBSET[5]:
            self.ambiguous_merged = True
            self.all_string = [x for x in string.ascii_letters if x not in AMBIGUOUS_LETTERS]
            for i in string.digits:
                self.all_string.append(i)
            self.training_path = '%s/all_digits_and_letters_train_path.csv' % DIRECTORY
            self.testing_path = '%s/all_digits_and_letters_test_path.csv' % DIRECTORY
        elif subset == SUBSET[6]:
            self.ambiguous_merged = False
            self.all_string = string.digits + string.ascii_lowercase + string.ascii_uppercase
            self.training_path = '%s/all_digits_and_letters_train_path.csv' % DIRECTORY
            self.testing_path = '%s/all_digits_and_letters_test_path.csv' % DIRECTORY
        self.outliers_path = '%s/outliers_to_remove.csv' % DIRECTORY
        self.training_df = pd.read_csv(self.training_path)
        self.testing_df = pd.read_csv(self.testing_path)
        if is_remove_outliers:
            pass
            # TODO : add outlier remover

    def one_hot_encoding(self, label):
        index = self.all_string.index(str(label))
        return np.asarray([1 if i == index else 0 for i in range(len(self.all_string))], dtype=np.float32)

    def train_next_batch(self, n=100):
        """
        only feeding training dataset
        :param n: batch size
        :return: tuple object of numpy array ( x=numpy.ndarray(float32), y=numpy.ndarray(float32) )
        """
        df_selected, self.training_df = self.training_df.iloc[:n], self.training_df.iloc[n:]
        length = len(df_selected)
        X = list()
        y = list()
        if length > 0:
            all_data = df_selected.values
            for row in all_data:
                file_path, label = row
                if self.ambiguous_merged:
                    if label in AMBIGUOUS_LETTERS:
                        label = label.lower()

                x = Dataset.read_png(file_path).flatten()
                y_ = self.one_hot_encoding(label)
                X.append(x)
                y.append(y_)
            return (np.asarray(X, dtype=np.float32), np.asarray(y, dtype=np.float32))
        return None

    def test_next_batch(self, n=100):
        """
        only feeding testing dataset
        :param n: batch size
        :return: tuple object of numpy array ( x=numpy.ndarray(float32), y=numpy.ndarray(float32) )
        """
        df_selected, self.testing_df = self.testing_df.iloc[:n], self.testing_df.iloc[n:]
        length = len(df_selected)
        X = list()
        y = list()
        if length > 0:
            all_data = df_selected.values
            for row in all_data:
                file_path, label = row
                if self.ambiguous_merged:
                    if label in AMBIGUOUS_LETTERS:
                        label = label.lower()

                x = Dataset.read_png(file_path).flatten()
                y_ = self.one_hot_encoding(label)
                X.append(x)
                y.append(y_)
            return (np.asarray(X, dtype=np.float32), np.asarray(y, dtype=np.float32))
        return None

    @staticmethod
    def read_png(file_path):
        """
        Read the png file and pre-process the data
        :param file_path: file path of the png file
        :return: a numpy array of the processed data
        """
        img = Image.open(file_path).convert("L")
        img = resizeimage.resize_crop(img, [80, 80])  # crop the image with a centered rectangle of the size 80x80
        inverted_image = PIL.ImageOps.invert(img)
        img_data = inverted_image.getdata()
        img_as_list = np.asarray(img_data, dtype=float)
        img_as_list = img_as_list.reshape(inverted_image.size)

        # normalize the entries of the np array
        for i in range(img_as_list.shape[0]):
            for j in range(img_as_list.shape[1]):
                if img_as_list[i][j] != 0:
                    img_as_list[i][j] = 1

        # pool the np array and reduce the size to 40x40
        result_list = np.zeros([img_as_list.shape[0] // 2, img_as_list.shape[1] // 2], dtype=float)
        for i in range(img_as_list.shape[0] // 2):
            for j in range(img_as_list.shape[1] // 2):
                result_list[i][j] = max(img_as_list[2*i][2*j], img_as_list[2*i+1][2*j], img_as_list[2*i][2*j+1], img_as_list[2*i+1][2*j+1])

        return np.asarray(result_list, dtype=np.float32)


class WarmUpPhoto(object):
    """
    This generate a dummy data for warm up the model
    data members:
        - width: width of the dummy picture
        - height: height of the dummy picture
        - channel: channel of the dummy picture, it is grayscale by default
        - num_of_class: number of class, that is, the dimension of the label vector
    member function:
        - generate_dummy_input: generate a dummy data with label, and return as np assay
    """
    def __init__(self, width, height, num_of_class, channel='gray'):
        """
        :param width: width of warmup photo
        :param height: height of warmup photo
        :param num_of_class: number of possible prediction
        :param channel: determine whether it is grayscale or RGB photo
        """
        self.width = width
        self.height = height
        self.channel = channel
        self.num_of_class = num_of_class

    def generate_dummy_input(self):
        """
        Generate a dummy data with label, and return as np assay
        :return: tuple object of numpy array ( x=numpy.ndarray(float32), y=numpy.ndarray(float32) )
        """
        if self.channel == 'gray':
            blank_photo = 0.1 * np.random.rand(self.height, self.width)
        elif self.channel == 'RGB':
            blank_photo = 0.1 * np.random.rand(self.height, self.width, 3)
        blank_photo_arr = np.asarray(blank_photo, dtype=np.float32).flatten()
        blank_label_arr = 0.1 * np.random.rand(self.num_of_class)
        blank_label = np.asarray(blank_label_arr, dtype=np.float32)
        X = list()
        y = list()
        X.append(blank_photo_arr)
        y.append(blank_label)
        return (np.asarray(X, dtype=np.float32), np.asarray(y, dtype=np.float32))


def get_photo_path(subset, row):
    """
    A helper function that finds the file paths of the wrong predicted data
    :param subset: subset of NIST that is trained and tested
    :param row: number of row in testing path csv files
    :return: the path of wrong predicted data
    """
    if subset == SUBSET[0]:
        test_path_prefix = TYPE[0]
    elif subset == SUBSET[1]:
        test_path_prefix = TYPE[1]
    elif subset == SUBSET[2]:
        test_path_prefix = TYPE[2]
    elif subset == SUBSET[3] or subset == SUBSET[4]:
        test_path_prefix = TYPE[3]
    elif subset == SUBSET[5] or subset == SUBSET[6]:
        test_path_prefix = TYPE[4]
    test_file_path = 'all_path_text_file/%s_test_path.csv' % test_path_prefix

    test_path_csv = open(test_file_path, 'r')
    test_path_csv.readline()  # ignore the header of the csv file
    lines = test_path_csv.readlines()
    photo_path, label = lines[row].split(',')
    return photo_path


def cnn_model(x, num_of_class):
    """
    The CNN model for NIST dataset classification
    :param x: input tensor with the dimensions [None, 1600]
    :param num_of_class: number of classes/possible outcomes
    :return: tuple (y_conv, keep_prob)
             where y_conv is the logits, keep_prob is the placeholder for the dropout rate
    """
    x_image = tf.reshape(x, [-1, 40, 40, 1])

    # input size: 40x40
    # first convolution layer
    #   - kernel size: 4x4
    #   - channel-in: 1
    #   - channel-out: 64
    # see the description of function 'conv2d' below for more detail
    W_conv1 = weight_variable([4, 4, 1, 64])
    b_conv1 = bias_variable([64])
    h_conv1 = tf.nn.relu(conv2d(x_image, W_conv1) + b_conv1)  # Relu activation

    # first pooling layer, see the description of function 'max_pool_2x2' below
    h_pool1 = max_pool_2x2(h_conv1)

    # input size: 20x20
    # second convolution layer
    #   - kernel size: 3x3
    #   - channel-in: 64
    #   - channel-out: 128
    # see the description of function 'conv2d' below for more detail
    W_conv2 = weight_variable([3, 3, 64, 128])
    b_conv2 = bias_variable([128])
    h_conv2 = tf.nn.relu(conv2d(h_pool1, W_conv2) + b_conv2)  # Relu activation

    # second pooling layer, see the description of function 'max_pool_2x2' below
    h_pool2 = max_pool_2x2(h_conv2)

    # input size: 10x10
    # third convolution layer
    #   - kernel size: 2x2
    #   - channel-in: 128
    #   - channel-out: 256
    # see the description of function 'conv2d' below for more detail
    W_conv3 = weight_variable([2, 2, 128, 256])
    b_conv3 = bias_variable([256])
    h_conv3 = tf.nn.relu(conv2d(h_pool2, W_conv3) + b_conv3)  # Relu activation

    # third pooling layer, see the description of function 'max_pool_2x2' below
    h_pool3 = max_pool_2x2(h_conv3)

    # input size: 5x5
    # fully connection layer 1
    W_fc1 = weight_variable([5 * 5 * 256, 2048])
    b_fc1 = bias_variable([2048])

    h_pool4_flat = tf.reshape(h_pool3, [-1, 5 * 5 * 256])
    h_fc1 = tf.nn.relu(tf.matmul(h_pool4_flat, W_fc1) + b_fc1)  # Relu activation

    keep_prob = tf.placeholder(tf.float32)
    h_fc1_drop = tf.nn.dropout(h_fc1, keep_prob)  # dropout for preventing overfitting

    # fully connection layer 2
    W_fc2 = weight_variable([2048, 512])
    b_fc2 = bias_variable([512])

    h_fc2 = tf.nn.relu(tf.matmul(h_fc1_drop, W_fc2) + b_fc2)  # Relu activation

    h_fc2_drop = tf.nn.dropout(h_fc2, keep_prob)  # dropout for preventing overfitting

    # fully connection layer 3
    W_fc3 = weight_variable([512, num_of_class])
    b_fc3 = bias_variable([num_of_class])

    y_conv = tf.matmul(h_fc2_drop, W_fc3) + b_fc3
    return y_conv, keep_prob


def conv2d(x, W):
    """
    conv2d returns a 2d convolution layer with full stride.
    """
    return tf.nn.conv2d(x, W, strides=[1, 1, 1, 1], padding='SAME')


def max_pool_2x2(x):
    """
    max_pool_2x2 downsamples a feature map by 2X.
    overlap pooling is done with kernel size 3x3, and strides 2x2
    """
    return tf.nn.max_pool(x, ksize=[1, 3, 3, 1],
                          strides=[1, 2, 2, 1], padding='SAME')


def weight_variable(shape):
    """
    weight_variable generates a weight variable of a given shape.
    """
    initial = tf.truncated_normal(shape, stddev=0.1)
    return tf.Variable(initial)


def bias_variable(shape):
    """
    bias_variable generates a bias variable of a given shape.
    """
    initial = tf.constant(0.1, shape=shape)
    return tf.Variable(initial)


def main(device, log_filename, subset, num_of_class, learning_rate=0.0009):
    """
    :param device: log which device is used
    :param log_filename: name of logging files
    :param subset: identify which subset of NIST is trained and tested
    :param num_of_class: number of possible outcomes of CNN model
    :param learning_rate: learning rate of CNN
    :return: None
    """

    start_program_time = time()
    data_feeder = Dataset(subset=subset)

    with tf.Graph().as_default(), tf.Session(config=tf.ConfigProto(log_device_placement=True)) as sess:
        with tf.device(device):

            # create the placeholder
            x = tf.placeholder(tf.float32, [None, 1600])
            y_ = tf.placeholder(tf.float32, [None, num_of_class])

            # inference
            y_conv, keep_prob = cnn_model(x, num_of_class)

            # loss
            cross_entropy = tf.reduce_mean(
                tf.nn.softmax_cross_entropy_with_logits(labels=y_, logits=y_conv))

            # train_op
            train_step = tf.train.AdamOptimizer(learning_rate).minimize(cross_entropy)  # overall test accuracy 0.859522

            # eval_correct
            correct_prediction = tf.equal(tf.argmax(y_conv, 1), tf.argmax(y_, 1))
            accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

            # create log files
            train_log_filename = '%s_train.csv' % log_filename
            test_log_filename = '%s_test.csv' % log_filename
            wrong_prediction_log_filename = '%s_wrong_prediction.csv' % log_filename
            execution_time_log_filename = '%s_execution_time.csv' % log_filename
            with open(train_log_filename, 'w') as file:
                file.write('step, loss, accuracy\n')
            with open(test_log_filename, 'w') as file:
                file.write('step, accuracy\n')
            with open(wrong_prediction_log_filename, 'w') as file:
                file.write('path, wrong prediction, actual answer\n')

            sess.run(tf.global_variables_initializer())

            # warm up the model
            warmup = WarmUpPhoto(40, 40, num_of_class)
            dummy_input = warmup.generate_dummy_input()
            for i in range(10):
                train_step.run(feed_dict={x: dummy_input[0], y_: dummy_input[1], keep_prob: 1.0})
                accuracy.eval(feed_dict={x: dummy_input[0], y_: dummy_input[1], keep_prob: 1.0})
                print('Warm-up for the %sth time' % i)
            print('Warm-up finish')

            # train the model
            start_training_time = time()
            batch = data_feeder.train_next_batch(100)
            i = 100
            while batch is not None:
                _, loss_value = sess.run([train_step, cross_entropy], feed_dict={x: batch[0], y_: batch[1], keep_prob: 0.5})
                if i % 100 == 0:
                    print('step %d, loss = %g' % (i, loss_value))
                    train_accuracy = accuracy.eval(feed_dict={x: batch[0], y_: batch[1], keep_prob: 1.0})
                    print('step %d, training accuracy %g' % (i, train_accuracy))
                    with open(train_log_filename, 'a') as file:
                        file.write('%s, %s, %s\n' % (i, loss_value, train_accuracy))
                batch = data_feeder.train_next_batch(100)
                i += 100
                if DEBUG_FLAG:
                    break
            end_training_time = time()

            test_size = list()
            test_acc = list()

            # test the model
            start_testing_time = time()
            test_batch = data_feeder.test_next_batch(100)
            i = 0
            while test_batch is not None:
                acc, prediction_class_array, actual_answer_array = sess.run([accuracy, y_conv, y_], feed_dict={x: test_batch[0], y_: test_batch[1], keep_prob: 1.0})
                prediction_array = correct_prediction.eval(feed_dict={x: test_batch[0], y_: test_batch[1], keep_prob: 1.0})

                # log down the info of wrong predicted data
                for j in range(len(test_batch[0])):
                    if not prediction_array[j]:
                        wrong_prediction_index = np.argmax(prediction_class_array[j])
                        wrong_prediction = data_feeder.all_string[wrong_prediction_index]
                        actual_answer_index = np.argmax(actual_answer_array[j])
                        actual_answer = data_feeder.all_string[actual_answer_index]
                        path = get_photo_path(subset, i + j)
                        with open(wrong_prediction_log_filename, 'a') as file:
                            file.write('%s, %s, %s\n' % (path, wrong_prediction, actual_answer))

                test_acc.append(acc)
                test_size.append(len(test_batch[0]))

                if i % 100 == 0:
                    print('%dth test accuracy %g' % (i, acc))
                    with open(test_log_filename, 'a') as file:
                        file.write('%s, %s\n' % (i, acc))
                test_batch = data_feeder.test_next_batch(100)
                i += 100
                if DEBUG_FLAG:
                    break
            end_testing_time = time()

            # calculate the overall accuracy
            all_test_acc = np.average(test_acc, weights=[float(i)/sum(test_size) for i in test_size])
            print('overall test accuracy %g' % all_test_acc)
            with open(test_log_filename, 'a') as file:
                file.write('Total accuracy: %s\n' % all_test_acc)

            end_program_time = time()

            # log down the execution time
            with open(execution_time_log_filename, 'w') as file:
                file.write('training time: %s\n' % (end_training_time - start_training_time))
                file.write('testing time: %s\n' % (end_testing_time - start_testing_time))
                file.write('Overall execution time: %s\n' % (end_program_time - start_program_time))

            # log down the detail of wrong predictions for further evaluation
            wrong_predict_evaluation(wrong_prediction_log_filename, data_feeder.all_string)


if __name__ == '__main__':
    main('/gpu:0', 'ForDebugging_digits', SUBSET[0], 10)
    main('/gpu:0', 'ForDebugging_uppercase_letters', SUBSET[1], 26)
    main('/gpu:0', 'ForDebugging_lowercase_letters', SUBSET[2], 26)
    main('/gpu:0', 'ForDebugging_letters_with_ambiguous_merged', SUBSET[3], 37)
    main('/gpu:0', 'ForDebugging_all_letters', SUBSET[4], 52)
    main('/gpu:0', 'ForDebugging_digits_and_ambiguous_merged_letters', SUBSET[5], 47)
    main('/gpu:0', 'ForDebugging_digits_and_letters', SUBSET[6], 62)
